/**
 * Created by sperez2017 on 08/03/2018.
 */
$("#filters").on("submit", function(event){
    event.preventDefault();

    $.ajax({
        url: $(this).attr("action"),
        type: "GET",
        data: $(this).serialize()
    })

    .done(function (response) {
        console.log(response);
        $("#last-movies").html($(response).find("#last-movies").html())
    })
});

$("#genre-filter").change(function (event) {
    event.preventDefault();
    $("#filters").submit();
});

$("#reset-filters").on("click", function (event) {
    event.preventDefault();
    $("#filters").trigger("reset");
    $("#filters").submit();
});
