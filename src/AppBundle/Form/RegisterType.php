<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', null, ["label" => "Email"])
                ->add('username', null, ["label" => "Pseudo"])
                ->add('password', RepeatedType::class, [
                    "label" => "Mot de passe",
                    "type" => PasswordType::class,
                    "first_options" => ["label" => "Mot de passe"],
                    "second_options" => ["label" => "Confirmer le mot de passe"],
                    "invalid_message" => "Les mots de passe ne correspondent pas"
                ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
