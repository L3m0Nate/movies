<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\RegisterType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class UserController extends Controller
{
    // Register new user
    public function registerAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();

        // New user form
        $registerForm = $this->createForm(RegisterType::class, $user);
        $registerForm->handleRequest($request);

        if ($registerForm->isSubmitted() && $registerForm->isValid()) {

            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);

            $user->setRoles("ROLE_USER");

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash("success", "Bienvenue!");

            return $this->redirectToRoute("home");
        }

        return $this->render('register.html.twig', [
            "registerForm" => $registerForm->createView()
        ]);
    }

    public function loginAction(Request $request, AuthenticationUtils $authUtils)
    {
        // En cas d'erreur de login
        $error = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();

        return $this->render("base.html.twig", [
            'last_username' => $lastUsername,
            "error" => $error
        ]);
    }

}
