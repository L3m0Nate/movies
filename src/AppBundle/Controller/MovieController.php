<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Genre;
use AppBundle\Entity\Movie;
use AppBundle\Entity\People;
use AppBundle\Entity\Review;
use AppBundle\Form\ReviewType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MovieController extends Controller
{
    public function homeAction(Request $request)
    {
        $movRepo = $this->getDoctrine()->getRepository(Movie::class);
        $genreRepo = $this->getDoctrine()->getRepository(Genre::class);

        $selectedGenre = $request->query->get('genre');
        $selectedMinYear = $request->query->get('minyear');
        $selectedMaxYear = $request->query->get('maxyear');
        $movies = $movRepo->findMoviesByGenreAndYear($selectedGenre, $selectedMinYear, $selectedMaxYear);

        // Year form select
        $oldest = $movRepo->findOneBy([], ["year" => "ASC"]);
        $years = [];
        for ($i = $oldest->getYear(); $i <= date('Y'); $i++) {
            $years[$i] = $i;
        }

        $genres = $genreRepo->findBy([]);

        return $this->render('home.html.twig', [
            "movies" => $movies,
            "genres" => $genres,
            "years" => $years,
            "oldestYear" => $oldest->getYear(),
            "currentYear" => date('Y')
        ]);
    }

    public function searchAction(Request $request)
    {
        return $this->render("search.html.twig");
    }


    public function detailsAction($id, Request $request)
    {
        $movRepo = $this->getDoctrine()->getRepository(Movie::class);
        $movie = $movRepo->findOneBy(["id" => $id]);

        // Review form
        $review = new Review();
        $reviewForm = $this->createForm(ReviewType::class, $review);
        $reviewForm->handleRequest($request);

        $reviewRepo = $this->getDoctrine()->getRepository(Review::class);
        $reviews = $reviewRepo->findBy([], ["dateCreated" => "DESC"]);

        // Validation
        if ($reviewForm->isSubmitted() && $reviewForm->isValid()) {

            $review->setUser($this->getUser());
            $review->setMovie($movie);

            $em = $this->getDoctrine()->getManager();
            $em->persist($review);
            $em->flush();

            $this->addFlash("success", "Votre critique a bien été postée!");

            return $this->redirectToRoute("details", ["id" => $movie->getId()]);
        }

        return $this->render('details.html.twig',[
            "movie" => $movie,
            "reviewForm" => $reviewForm->createView(),
            "reviews" => $reviews
        ]);
    }

}
